#ifndef GENERATOR_H
#define GENERATOR_H

#include <QtCore>

class Generator
{
public:
    Generator(QString input, QString output);

    bool process();

private:
    QString in;
    QString out;
};

#endif // GENERATOR_H
