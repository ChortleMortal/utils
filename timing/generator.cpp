#include "generator.h"
#include <iostream>
using std::cout;

Generator::Generator(QString input, QString output)
{
    in = input;
    cout << "input : " << input.toStdString() << std::endl;

    out = output;
    cout << "output: " <<  output.toStdString() << std::endl;
}

bool Generator::process()
{
    QFile infile(in);
    if (!infile.open(QFile::ReadOnly))
    {
        cout << "Error: could not open " << in.toStdString() << std::endl;
        return false;
    }

    QFileInfo fi(in);
    QString path = fi.path();

    QFile outfile(path + "/" + out);
    if (!outfile.open(QFile::WriteOnly | QFile::Truncate))
    {
        cout << "Error: could not open " << out.toStdString() << std::endl;
        infile.close();
        return false;
    }

    QTextStream ins(&infile);
    QTextStream outs(&outfile);

    while (!ins.atEnd())
    {
        QString line = ins.readLine();
        if (line.contains("took"))
        {
            line.remove("Info    : Load operation for ");
            line.replace("took",",");
            line.remove("seconds");
            outs << line << Qt::endl;
        }
        //QStringList fields = line.split(",");
        //model->appendRow(fields);
    }

    infile.close();
    outfile.close();
    return true;
}
